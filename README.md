
  

# PI_Digit_Recognition

  

## Purpose

  

This service provides grid structure recognition for handwritten and computed Hashiwokakero game grids.

  

  

## Installation

  

### Using docker

The service is ready to be deployed in a docker container:

  

``` ./deploy.sh <root password> ```

  

  

### With python

You can also run the service with python:

Install dependencies:

``` pip install py-requirements.txt```

Run the service:

``` cd src/ ```

``` python main.py```

## How tu use

To use this service, you have to POST a list of jpeg image encoded in base64, to <ip_adress>:50002/digit.

The service returns a JSON with the following content:

```json

{
	"grid": {
		"dimensions": <int>,
		"circles": [
			{"position": <int>, "img": [<int>, <int>]},
			...
			{"position": <int>, "img": [<int>, <int>]},
		]
	}
}

```