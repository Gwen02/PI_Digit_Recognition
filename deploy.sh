#!/bin/bash

# Pull the repository

sudo -u user git pull

# stop / rm the old running docker
echo -e $1 | sudo -S docker stop digit_reco_ctn
echo -e $1 | sudo -S docker rm digit_reco_ctn

# Generate the new docker image
echo -e $1 | sudo -S docker build --network=host . -t digit_reco_img

# Run the docker
echo -e $1 | sudo -S docker run -dp 50002:50002 --name digit_reco_ctn digit_reco_img
