# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 11:17:07 2021

@author: xSyl0
"""

import numpy as np 
import cv2
import pickle
import json
import sys
import cntk

from skimage import measure
from scipy import ndimage


class Digit:
    def __init__(self, file, resize_width, key):
        
        self.digit = None #Recognition of digit
        self.digits = None
        self.digit_value = None #Value of filtered digit
        self.resize_width = resize_width
        self.key = key
        
        self.img = file
        self.bw_img = []
        self.labelised_img = None
        self.cleaned_img = np.zeros(28*28).reshape(28,28)
        self.final_img = np.zeros(28*28).reshape(28,28)
        
        self.center_x = int(resize_width / 2)
        self.center_y = int(resize_width / 2)
        
        self.indexes = None #Indexes of of digit's pixels
        self.couples = [] #Position of pixels
        
        self.network = pickle.load(open("./finalized_model.sav", 'rb'))
        #self.network2 = cntk.Function.load('mnist-8.onnx', format=cntk.ModelFormat.ONNX)
        self.network2 = cntk.Function.load('Custom_CNN_MNIST.onnx', format=cntk.ModelFormat.ONNX)
        self.out = self.network2
        self.proba_list = None
          
        self.__px_width = resize_width
        
        self._resize_img()   
        
        self._process_label()
        
    def _resize_img(self):
        self.img = cv2.resize(self.img, (self.resize_width, self.resize_width))
        self._to_grayscale()
        
    def _to_grayscale(self):
        for row in self.img:
            for value in row:
                if value > 122:
                    self.bw_img.append(0)
                else:
                    self.bw_img.append(255)
        self.bw_img = np.reshape(np.array(self.bw_img), (28,28))
    
    def _process_label(self):
        self.labelised_img = measure.label(self.bw_img)
        
    def find_center_value(self):
        #Find the value of the pixel that's closest to the center
        arr = self.labelised_img
        x, y = int(np.shape(arr)[0] / 2), int(np.shape(arr)[1] / 2)
        if(arr[(x, y)] == 0):
            for i in range(int(self.resize_width / 2)):
                neigh_list = [arr[(x + i, y)], arr[(x - i, y)], arr[(x, y + i)], arr[(x, y -i)],
                         arr[(x + i, y + i)], arr[(x + i, y - i)], arr[(x - i, y + i)], arr[(x - i, y - i)]]
                for value in neigh_list:
                    if (value != 0):
                        self.digit_value = value
                        self.indexes = np.where(arr == self.digit_value)
                        self.clean_img()
                        return
        else:
            self.digit_value = arr[(x, y)]  
            self.indexes = np.where(arr == self.digit_value)
            self.clean_img()
            return
            
    def clean_img(self):
        for i in range(np.shape(self.indexes)[1]):
            self.couples.append((self.indexes[0][i], self.indexes[1][i]))            
        for couple in self.couples:
            self.cleaned_img[couple] = 255
            
    def center_img(self):
        center_of_mass = ndimage.measurements.center_of_mass(self.cleaned_img)
        mass = (int(center_of_mass[0]), int(center_of_mass[1]))
        centered_couple = []
        diff_x, diff_y = self.center_x - mass[1] , self.center_y - mass[0] 
        for (x, y) in self.couples:                
            centered_couple.append((x+diff_y, y+diff_x))
        for couple in self.couples:
            self.final_img[couple] = 255
        self.final_img = np.reshape(self.final_img, (1,-1))

    def predict_img(self):
        self.digit = self.network.predict(np.reshape(self.cleaned_img, (1,-1)))
        self.proba_list = list(self.network.predict_proba(self.final_img)[0])
        print(self.proba_list)
   
    def generate_tuple(self):
        print("generate tuple")
        predicted = self.out.eval([np.reshape(self.final_img, (1,1,28,28))])
        print("Predicted values : " + str(predicted), file=sys.stderr)

        pred_filter = np.delete(predicted, 9)
        pred_filter = np.delete(pred_filter, 0)

        print(f"Pred_filter = {pred_filter}", file=sys.stderr)
        # first digit
        max_index = np.argmax(pred_filter)
        proba_1 = pred_filter[max_index]
        self.digits = [max_index+1]

        print(f"pred_filter = {pred_filter}", file=sys.stderr)

        pred_filter[max_index] = np.min(pred_filter)

        # second digit
        max_index = int(np.argmax(pred_filter))
        proba_2 = pred_filter[max_index]
        self.digits.append(max_index+1)
        
        dict = {}
        dict["position"] = self.key
        dict["digit"] = [None]*2
        dict["digit"][0] = {}
        dict["digit"][0]["val"] = int(self.digits[0])
        dict["digit"][0]["proba"] = int(proba_1)
        dict["digit"][1] = {}
        dict["digit"][1]["val"] = int(self.digits[1])
        dict["digit"][1]["proba"] = int(proba_2)
        print(f"dict = {dict}", file=sys.stderr)
        return dict

        #self.digits.append(0)
        #print(self.digits, file=sys.stderr)
        #return (self.key, self.digits)
            