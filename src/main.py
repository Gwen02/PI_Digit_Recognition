# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 11:33:18 2021

@author: xSyl0
"""

import digit_recognition as drecognizer
import hashitools as ht
import json
import sys

from flask import Flask, request, jsonify, Response
from werkzeug.exceptions import HTTPException


########## REST API ##########
app = Flask(__name__)


@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(code=code, error=str(e)), code

@app.route('/digit', methods=['POST'])
def predict_digit():
    # Decode the base64 img (-> cv2 image)
    # Recognizer
    ## Creation
    try:
        loaded_json = request.json
        data = loaded_json['grid']['circles']
        img, pos = [], []
        for i in range(len(data)):
            img.append(ht.b64_to_cv(data[i]['img']))
            pos.append(data[i]['position'])
        tuple_list = []

        output_json = {}
        output_json['grid'] = {}
        output_json['grid']['dimensions'] = loaded_json['grid']['dimensions']
        output_json['grid']['circles'] = []

        i = 0
        for (v, k) in zip(pos,img):
            reco = drecognizer.Digit(k, 28, v)
            reco.find_center_value()
            reco.clean_img()
            reco.center_img()
            reco.predict_img()        
            output_json['grid']['circles'].append(reco.generate_tuple())
            i+=1
            
            #tuple_list.append(reco.generate_tuple())
            #print(type(tuple_list[0][1][0]))
     
        
         #for i in range(len(tuple_list)):
         #    output_json['grid']['circles'][i] = {}
         #    output_json['grid']['circles'][i]['position'] = tuple_list[i][0]
         #    output_json['grid']['circles'][i]['digits'] = tuple_list[i][1]
    except Exception as e:
        print(e, file=sys.stderr) 

    return Response(json.dumps(output_json), mimetype='application/json')   
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=50002)