# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 16:35:47 2022

@author: xSyl0
"""

import unittest
import os 
import sys
import json
import cv2
import pickle
from PIL import Image, ImageOps
# Import files
sys.path.append("../")
sys.path.append("../../")
import digit_recognition
import numpy as np


class DigitRecoTest(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName=methodName)
        self.digits = [x for x in os.listdir("test_pics") if x.endswith(".jpg")]
        self.expected = json.load(open("test_pics/expected_results2.json", "r"))
   
    def test_digit_reco(self):  
        ent = 1
        for dn in self.digits:
            #img=Image.open(f"test_pics/{dn}").resize((28,28), resample=0)
            img = cv2.imread(f"test_pics/{dn}", 0)
            reco = digit_recognition.Digit(img, 28, str(ent))      
            reco.find_center_value()
            reco.clean_img()
            reco.center_img()
            reco.predict_img() 
            reco.generate_json()
            self.assertEqual(reco.digits, \
                             self.expected["expected"][str(ent)]["digits"]) 
            ent+=1
  
if __name__ == '__main__':
    unittest.main()